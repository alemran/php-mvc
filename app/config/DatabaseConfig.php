<?php

namespace App\config;

use App\utilities\DynamicDBConfig;

class DatabaseConfig
{

    private $host = "localhost";
    private $database = "";
    private $username = "";
    private $password = "";
    public $conn;
    public $connectionStatus = '';
    private static $instances = [];

    public function getConnection()
    {
        $this->setConfig();
        try {
            $this->conn = new \PDO("mysql:host=localhost;dbname=" . $this->database, $this->username, $this->password);
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->connectionStatus = "Connected";
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            //  return redirect('databaseSetup');
            $this->connectionStatus = "Not Connected";
        }


    }


//    protected function __construct() { }
//
//    /**
//     * Singletons should not be cloneable.
//     */
//    protected function __clone() { }
//
//    /**
//     * Singletons should not be restorable from strings.
//     */
//    public function __wakeup()
//    {
//        throw new \Exception("Cannot unserialize a singleton.");
//    }

      function __construct()
    {
        $this->getConnection();
    }

    public function setConfig()
    {
        $config = DynamicDBConfig::readEnvFile();

        if (isset($config['database']) && $config['database']) {
            $this->database = $config['database'];
            $this->username = $config['user'];
            $this->password = $config['password'];

        }
    }

    public static function getInstance(): Singleton
    {
        $cls = static::class;
        if (!isset(static::$instances[$cls])) {

            static::$instances[$cls] = new static;
        }

        return static::$instances[$cls];
    }


}