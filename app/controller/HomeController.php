<?php


namespace App\controller;


use App\models\DBQuery;
use App\models\Purchase;
use App\utilities\DynamicDBConfig;
use App\utilities\Validator;
use System\Request\Request;

class HomeController
{

    public function home(Request $r)
    {

        if (!DynamicDBConfig::checkEnv()) {
            return redirect('/databaseSetup');
        }
        $purchase = new Purchase();
        $purchaseData = $purchase->getAll();

        return view('index', compact('purchaseData'));
    }


}