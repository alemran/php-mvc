<?php

namespace System\Request;

class Request implements RequestInterface
{

    function __construct()
    {
        $this->requestInit();
    }

    /**
     *
     * set all requested data as this class property;
     */
    public function requestInit()
    {
        $this->serverDataSet();
        $this->postDataSet();
        $this->getDataSet();
    }


    /**
     *set server global data as this class property
     */
    public function serverDataSet()
    {
        foreach ($_SERVER as $key => $value) {
            $this->{$this->makeCamelCase($key)} = $value;
        }
    }



    /**
     *set server global data as this class property
     */
    public function postDataSet()
    {
        foreach ($_POST as $key => $value) {
            $this->{$this->makeCamelCase($key)} = $value;
        }

    }



    /**
     *set server global data as this class property
     */
    public function getDataSet()
    {
        foreach ($_GET as $key => $value) {
            $this->{$this->makeCamelCase($key)} = $value;
        }

    }


    /**
     * make camel case
     * @param $string
     * @return mixed|string
     */
    public function makeCamelCase($string)
    {
        $result = strtolower($string);

        preg_match_all('/_[a-z]/', $result, $matches);
        foreach ($matches[0] as $match) {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }
        return $result;
    }


    /**
     *  get data from request
     * @param $name
     * @return string
     */

    public function get($name)
    {
        if ($this->requestMethod == "POST") {
            return $_POST[$name] ?? null;

        } else if ($this->requestMethod == "GET") {
            return $_GET[$name] ?? null;
        }
        return null;
    }


    public function __get($property)
    {
        return isset($this->{$property}) ? $this->{$property} : null;
    }


}