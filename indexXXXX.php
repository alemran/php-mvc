<?php

require_once 'app/system/bootstrap/Start.php';

use App\controller\PurchaseController;
use App\models\Purchase;
use Route\Request;
use Route\Router;
use App\controller\DatabaseSetup;
//$purchase = new Purchase();


$router = new Router(new Request);


 $router->get('/', 'HomeController@home');


//$router->get('/', function() {
//    $controller =  new HomeController(new Request) ;
//   return $controller->home();
//});

$router->post('/saveData', function($request) {
    $controller =  new PurchaseController($request) ;
   return $controller->saveData();
});



$router->get('/databaseSetup', function($request)   {
    $controller =  new DatabaseSetup($request) ;
    return $controller->getDBForm();
});


$router->post('/databaseSetup', function($request)   {
    $controller =  new DatabaseSetup($request) ;
    return $controller->saveDBConfig();
});




