<?php

$router->get('/', 'HomeController@home');

$router->post('/saveData', 'PurchaseController@saveData');


$router->get('/test', function()   {
    echo 'Test Done';
    return;
});


$router->get('/databaseSetup', function()   {
    $controller =  new App\controller\DatabaseSetup ;
    return $controller->getDBForm(new \System\Request\Request);
});


$router->post('/databaseSetup', function()   {
    $controller =  new DatabaseSetup() ;
    return $controller->saveDBConfig();
});


